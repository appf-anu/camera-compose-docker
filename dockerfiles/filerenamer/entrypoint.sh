#!/bin/bash
set -u

INFOJSON='{"level_number": 20, "level_name": "info"}'
function info() {
    time=$(date -Is)
    for msg in "$@"; do
        echo $INFOJSON | jq --compact-output --arg msg "$msg" --arg ts "$time" '. + {"time": $ts, "msg": $msg}'
    done
}
DEBUGJSON='{"level_number": 10, "level_name": "debug"}'
function debug() {
    time=$(date -Is)
    for msg in "$@"; do
        echo $DEBUGJSON | jq --compact-output --arg msg "$msg" --arg ts "$time" '. + {"time": $ts, "msg": $msg}'
    done
}

while true
do
    find /data/ -type f -name "*.${EXT}" -mmin +3 -not -path "*renamed*" -not -path '*/\.*' -print0 | while read -d $'\0' f
    do
        dir=$(dirname $(readlink -f "$f"))
        mkdir -p "$dir/renamed"
        time_mod=$(date "+%s" -d @$(stat -c '%Y' "$f"))
        time_trunc=$(($time_mod - ($time_mod % (10 * 60))))
        newfn=$(date +"$dir/renamed/%Y_%m_%d_%H_%M_00.jpg" -d @$time_trunc)
        mv "$f" "$newfn"
        debug "finished $f -> $newfn"
    done
    debug "done this loop, waiting for $INTERVAL_SECONDS"
    sleep $INTERVAL_SECONDS
done
