#!/bin/bash
set -u

INFOJSON='{"level_number": 20, "level_name": "info"}'
function info() {
    time=$(date -Is)
    for msg in "$@"; do
        echo $INFOJSON | jq --compact-output --arg msg "$msg" --arg ts "$time" '. + {"time": $ts, "msg": $msg}'
    done
}
DEBUGJSON='{"level_number": 10, "level_name": "debug"}'
function debug() {
    time=$(date -Is)
    for msg in "$@"; do
        echo $DEBUGJSON | jq --compact-output --arg msg "$msg" --arg ts "$time" '. + {"time": $ts, "msg": $msg}'
    done
}

cmdvars=( $(env | grep '^RCLONE_CMD' | cut -f 1 -d =) )

while true
do
    for cmdvar in "${cmdvars[@]}"
    do
        cmd=${!cmdvar}
        debug "running ${cmdvar} -> ${cmd}"
        $cmd
    done
    debug "done this loop, waiting for $INTERVAL_SECONDS"
    sleep $INTERVAL_SECONDS
done
