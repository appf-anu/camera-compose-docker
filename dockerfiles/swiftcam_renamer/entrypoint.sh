#!/bin/bash
set -u

INFOJSON='{"level_number": 20, "level_name": "info"}'
function info() {
    time=$(date -Is)
    for msg in "$@"; do
        echo $INFOJSON | jq --compact-output --arg msg "$msg" --arg ts "$time" '. + {"time": $ts, "msg": $msg}'
    done
}
DEBUGJSON='{"level_number": 10, "level_name": "debug"}'
function debug() {
    if [ "${DEBUG:-no}" == "no" ]
    then
        return
    fi
    time=$(date -Is)
    for msg in "$@"; do
        echo $DEBUGJSON | jq --compact-output --arg msg "$msg" --arg ts "$time" '. + {"time": $ts, "msg": $msg}'
    done
}

while true
do
    mkdir -p "${DIR}/renamed/"
    find "${DIR}" -type f -name "*.${EXT}" -mmin +3 -not -path "*renamed*" -not -path '*/\.*' -print0 | swiftcam-renamer "${DIR}/renamed/"
    debug "done this loop, waiting for $INTERVAL_SECONDS"
    sleep $INTERVAL_SECONDS
done
